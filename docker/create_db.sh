#!/bin/zsh

# run docker
docker run --name=postgres_db -d \
-e POSTGRES_DB=users_db \
-e POSTGRES_USER=student \
-e POSTGRES_PASSWORD=student \
-v /tmp/postgres-data:/var/lib/postgresql/data \
-v "/Users/vitalijkolesnik/Projects/study-microservices/user-service/Docker/init_scripts":/docker-entrypoint-initdb.d \
-p 5001:5432 \
postgres:15
