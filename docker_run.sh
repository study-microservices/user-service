#!/bin/zsh

# build jar
./gradlew clean assemble

# build image
docker build -t user-service .

# run docker
docker-compose up -d && docker-compose logs -f

