package com.skillbox.microservices.users.smoke;

import com.skillbox.microservices.users.containers.PostgresContainerWrapper;
import com.skillbox.microservices.users.entity.User;
import com.skillbox.microservices.users.enums.Gender;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Testcontainers(disabledWithoutDocker = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("integTest")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
public class UserControllerSmokeTest {

    public static final String HOST_URL = "http://localhost:%s";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Container
    private static final PostgreSQLContainer<PostgresContainerWrapper> postgresContainer = new PostgresContainerWrapper();

    @DynamicPropertySource
    public static void initSystemParams(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgresContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgresContainer::getUsername);
        registry.add("spring.datasource.password", postgresContainer::getPassword);
    }

    @Test
    void createUserTest() {
        String url = String.format(HOST_URL, port);
        User user = new User();
        user.setFirstName("Степан");
        user.setLastName("Никаноров");
        user.setSurName("Петрович");
        user.setGender(Gender.MALE);
        user.setBirthDate(LocalDate.of(1992, 12, 10));
        user.setNikname("stepan");
        user.setImageLink("https://dummyimage.com/165x165/c71090/fff.png");

        ResponseEntity<String> response = restTemplate.postForEntity(url + "/users/", user, String.class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response.getBody());

        String userId = response.getBody().split("=")[1].trim();

        ResponseEntity<User> getResponse = restTemplate.getForEntity(url + "/users/" + userId, User.class);
        assertEquals(200, getResponse.getStatusCodeValue());

        user.setId(Long.parseLong(userId));

        assertEquals(user, getResponse.getBody());
    }

}
