package com.skillbox.microservices.users.controller;

import com.skillbox.microservices.users.containers.PostgresContainerWrapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.server.ResponseStatusException;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static com.skillbox.microservices.users.utils.TestUtils.classpathFileToString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Testcontainers(disabledWithoutDocker = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("integTest")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
class UserControllerTest {

    @Container
    private static final PostgreSQLContainer<PostgresContainerWrapper> postgresContainer = new PostgresContainerWrapper();

    @DynamicPropertySource
    public static void initSystemParams(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgresContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgresContainer::getUsername);
        registry.add("spring.datasource.password", postgresContainer::getPassword);
    }

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @BeforeEach
    private void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void createUser() throws Exception {
        String request = classpathFileToString("/mvc-requests/create_user.json");
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
        );
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",
                        equalTo("Пользователь Свиридов Николай Иванович добавлен с id = 4")));
    }

    @Test
    void createUserIfExists() throws Exception {
        String request = classpathFileToString("/mvc-requests/create_user.json");
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
        );
        String expected = String.format("400 BAD_REQUEST \"%s\"", "Пользователь Свиридов Николай Иванович уже существует");
        resultActions.andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> assertEquals(expected, result.getResolvedException().getMessage()));
    }

    @Test
    @SneakyThrows
    void getAllUsers() {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/users/list")
                .contentType(MediaType.APPLICATION_JSON)
        );
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].firstName", equalTo("Иван")))
                .andExpect(jsonPath("$[0].lastName", equalTo("Иванов")))
                .andExpect(jsonPath("$[1].firstName", equalTo("Петр")))
                .andExpect(jsonPath("$[1].lastName", equalTo("Петров")))
                .andExpect(jsonPath("$[2].firstName", equalTo("Наталья")))
                .andExpect(jsonPath("$[2].lastName", equalTo("Никанорова")));
    }

    @Test
    void getUser() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .get("/users/1")
        );
        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", equalTo("Иван")))
                .andExpect(jsonPath("$.lastName", equalTo("Иванов")));
    }


    @Test
    void updateUser() throws Exception {
        String request = classpathFileToString("/mvc-requests/update_user.json");
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .put("/users/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
        );
        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", equalTo("Пользователь с id = 1 успешно обновлен")));
    }

    @Test
    void subscribeToUser() throws Exception {
        String request = classpathFileToString("/mvc-requests/subscribe_user.json");
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/users/1/subscribe")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
        );
        String expected = "Пользователь Петров Петр Петрович подписался на пользователя с id = 1";
        resultActions
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$", equalTo(expected)));
    }

    @Test
    void unsubscribeFromUser() throws Exception {
        String request = classpathFileToString("/mvc-requests/subscribe_user.json");
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .post("/users/1/unsubscribe")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request)
        );
        String expected = "Пользователь Петров Петр Петрович отписался от пользователя с id = 1";
        resultActions
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$", equalTo(expected)));
    }

    @Test
    void deleteUser() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete("/users/1")
        );
        resultActions
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$", equalTo("Пользователь с id = 1 успешно удален")));
    }

    @Test
    void deleteUserNotFound() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete("/users/5")
        );
        String expected = String.format("404 NOT_FOUND \"%s\"", "Пользователь с id = 5 не найден");
        resultActions
                .andExpect(status().is4xxClientError())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> assertEquals(expected, result.getResolvedException().getMessage()));
    }

}