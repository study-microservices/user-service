package com.skillbox.microservices.users.regress;

import com.skillbox.microservices.users.containers.PostgresContainerWrapper;
import com.skillbox.microservices.users.entity.User;
import com.skillbox.microservices.users.enums.Gender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Testcontainers(disabledWithoutDocker = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("integTest")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
public class UserControllerRegressTest {

    public static final String HOST_URL = "http://localhost:%s";

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Container
    private static final PostgreSQLContainer<PostgresContainerWrapper> postgresContainer = new PostgresContainerWrapper();

    private String url;

    @DynamicPropertySource
    public static void initSystemParams(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgresContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgresContainer::getUsername);
        registry.add("spring.datasource.password", postgresContainer::getPassword);
    }

    @BeforeEach
    void init() {
        url = String.format(HOST_URL, port);
    }

    @Test
    void updateUserTest() {
        User user = new User();
        user.setFirstName("Степан");
        user.setLastName("Никаноров");
        user.setSurName("Петрович");
        user.setGender(Gender.MALE);
        user.setBirthDate(LocalDate.of(1992, 12, 10));
        user.setNikname("stepan");
        user.setImageLink("https://dummyimage.com/165x165/c71090/fff.png");
        ResponseEntity<String> response = restTemplate.postForEntity(url + "/users/", user, String.class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response.getBody());
        String userId = response.getBody().split("=")[1].trim();
        // change data
        user.setId(Long.parseLong(userId));
        user.setFirstName("Владимир");

        HttpEntity<User> request = new HttpEntity<>(user);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url + "/users/" + userId, HttpMethod.PUT, request, String.class);
        assertEquals(200, responseEntity.getStatusCodeValue());
        assertNotNull(responseEntity.getBody());
        String expected = String.format("Пользователь с id = %s успешно обновлен", userId);
        assertEquals(expected, responseEntity.getBody());
    }

    @Test
    void deleteUserTest() {
        User user = new User();
        user.setFirstName("Наталья");
        user.setLastName("Миллер");
        user.setSurName("Степановна");
        user.setGender(Gender.FEMALE);
        user.setBirthDate(LocalDate.of(1976, 5, 18));
        user.setNikname("nataly_miller");
        user.setImageLink("https://dummyimage.com/165x165/c71090/fff.png");
        ResponseEntity<String> response = restTemplate.postForEntity(url + "/users/", user, String.class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response.getBody());
        String userId = response.getBody().split("=")[1].trim();

        restTemplate.delete(url + "/users/" + userId);
        ResponseEntity<User> getResponse = restTemplate.getForEntity(url + "/users/" + userId, User.class);
        assertEquals(404, getResponse.getStatusCodeValue());
    }

    @Test
    void subscribeUserTest() {
        User user = new User();
        user.setFirstName("Светлана");
        user.setLastName("Столярова");
        user.setSurName("Игоревна");
        user.setGender(Gender.FEMALE);
        user.setBirthDate(LocalDate.of(1976, 5, 18));
        user.setNikname("sveta_stolyarova");
        user.setImageLink("https://dummyimage.com/165x165/c71090/fff.png");
        ResponseEntity<String> response = restTemplate.postForEntity(url + "/users/", user, String.class);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response.getBody());
        String userId = response.getBody().split("=")[1].trim();

        User user2 = new User();
        user2.setFirstName("Николай");
        user2.setLastName("Свиридов");
        user2.setSurName("Сергеевич");
        user2.setGender(Gender.MALE);
        user2.setBirthDate(LocalDate.of(1973, 4, 22));
        user2.setNikname("nickolay_sviridov");
        user2.setImageLink("https://dummyimage.com/165x165/c71090/fff.png");
        // create other user
        ResponseEntity<String> postResponse = restTemplate.postForEntity(url + "/users/", user2, String.class);
        assertEquals(200, postResponse.getStatusCodeValue());
        assertNotNull(postResponse.getBody());
        String userId2 = postResponse.getBody().split("=")[1].trim();
        user2.setId(Long.parseLong(userId2));

        ResponseEntity<String> subscribeResponse = restTemplate.postForEntity(url + "/users/" + userId + "/subscribe", user2, String.class);
        assertEquals(200, subscribeResponse.getStatusCodeValue());
        String expected = String.format("Пользователь %s подписался на пользователя с id = %s", user2.getFullName(), userId);
        assertEquals(expected, subscribeResponse.getBody());

        ResponseEntity<String> unsubscribeResponse = restTemplate.postForEntity(url + "/users/" + userId + "/unsubscribe", user2, String.class);
        assertEquals(200, unsubscribeResponse.getStatusCodeValue());
        expected = String.format("Пользователь %s отписался от пользователя с id = %s", user2.getFullName(), userId);
        assertEquals(expected, unsubscribeResponse.getBody());
    }

}
