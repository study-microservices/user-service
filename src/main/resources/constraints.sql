alter table users_scheme.user_addresses
    add constraint user_addresses_fk
        foreign key (address_id) references users_scheme.addresses(id),
    add constraint user_addresses_users_fk
        foreign key (user_id) references users_scheme.users(id);

alter table users_scheme.user_skills
    add constraint user_skills_fk
        foreign key (skill_id) references users_scheme.skills(id),
    add constraint user_skills_users_fk
        foreign key (user_id) references users_scheme.users(id);

alter table users_scheme.subscriptions
    add constraint subscriptions_user_fk
        foreign key (user_id) references users_scheme.users(id),
    add constraint subscriptions_subscription_fk
        foreign key (subscription_id) references users_scheme.users(id);

alter table users_scheme.subscribers
    add constraint subscribers_user_fk
        foreign key (user_id) references users_scheme.users(id),
    add constraint subscribers_subscriber_fk
        foreign key (subscriber_id) references users_scheme.users(id);
    