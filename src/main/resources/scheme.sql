-- Sequences

create sequence IF NOT EXISTS users_scheme.user_contacts_id_seq;
create sequence IF NOT EXISTS users_scheme.addresses_id_seq;
create sequence IF NOT EXISTS users_scheme.skills_id_seq;
create sequence IF NOT EXISTS users_scheme.user_addresses_id_seq;
create sequence IF NOT EXISTS users_scheme.user_id_seq;
create sequence IF NOT EXISTS users_scheme.user_skills_id_seq;
create sequence IF NOT EXISTS users_scheme.subscriptions_id_seq;
create sequence IF NOT EXISTS users_scheme.subscribers_id_seq;

-- Table: users_scheme.users

-- DROP TABLE users_scheme.users;

CREATE TABLE IF NOT EXISTS users_scheme.users
(
    id bigint NOT NULL DEFAULT nextval('users_scheme.user_id_seq'::regclass),
    first_name character varying(30) COLLATE pg_catalog."default" NOT NULL,
    last_name character varying(30) COLLATE pg_catalog."default" NOT NULL,
    surname character varying(30) COLLATE pg_catalog."default",
    gender char(1) NOT NULL,
    birth_date date,
    image_link character varying(255) COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    nikname character varying(30) COLLATE pg_catalog."default" NOT NULL,
    deleted boolean not null default false,
    CONSTRAINT users_pkey PRIMARY KEY (id)
)   TABLESPACE pg_default;

ALTER TABLE users_scheme.users
    OWNER to student;

-- Table: users_scheme.user_contacts

-- DROP TABLE users_scheme.user_contacts;

CREATE TABLE IF NOT EXISTS users_scheme.user_contacts
(
    id integer NOT NULL DEFAULT nextval('users_scheme.user_contacts_id_seq'::regclass),
    contact_type integer NOT NULL,
    contact character varying(30) COLLATE pg_catalog."default" NOT NULL,
    user_id bigint NOT NULL,
    CONSTRAINT user_contacts_pkey PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE users_scheme.user_contacts
    OWNER to student;

-- Table: users_scheme.skills

-- DROP TABLE users_scheme.skills;

CREATE TABLE IF NOT EXISTS users_scheme.skills
(
    id integer NOT NULL DEFAULT nextval('users_scheme.skills_id_seq'::regclass),
    name character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT skills_pkey PRIMARY KEY (id)
)
    TABLESPACE pg_default;

ALTER TABLE users_scheme.skills
    OWNER to student;

-- Table: users_scheme.addresses

-- DROP TABLE users_scheme.addresses;

CREATE TABLE IF NOT EXISTS users_scheme.addresses
(
    id integer NOT NULL DEFAULT nextval('users_scheme.addresses_id_seq'::regclass),
    city character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT addresses_pkey PRIMARY KEY (id)
)
    TABLESPACE pg_default;

ALTER TABLE users_scheme.addresses
    OWNER to student;

-- Table: users_scheme.user_addresses

-- DROP TABLE users_scheme.user_addresses;

CREATE TABLE IF NOT EXISTS users_scheme.user_addresses
(
    id integer NOT NULL DEFAULT nextval('users_scheme.user_addresses_id_seq'::regclass),
    user_id bigint NOT NULL,
    address_id integer NOT NULL,
    CONSTRAINT user_addresses_pkey PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE users_scheme.user_addresses
    OWNER to student;

-- Table: users_scheme.user_skills

-- DROP TABLE users_scheme.user_skills;

CREATE TABLE IF NOT EXISTS users_scheme.user_skills
(
    id integer NOT NULL DEFAULT nextval('users_scheme.user_skills_id_seq'::regclass),
    user_id bigint NOT NULL,
    skill_id integer NOT NULL,
    CONSTRAINT user_skills_pkey PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE users_scheme.user_skills
    OWNER to student;

-- Table: users_scheme.subscriptions

-- DROP TABLE users_scheme.subscriptions;

CREATE TABLE IF NOT EXISTS users_scheme.subscriptions
(
    id bigint NOT NULL DEFAULT nextval('users_scheme.subscriptions_id_seq'::regclass),
    user_id bigint NOT NULL,
    subscription_id bigint NOT NULL,
    CONSTRAINT subscriptions_pkey PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE users_scheme.subscriptions
    OWNER to student;

-- Table: users_scheme.subscribers

-- DROP TABLE users_scheme.subscribers;

CREATE TABLE IF NOT EXISTS users_scheme.subscribers
(
    id bigint NOT NULL DEFAULT nextval('users_scheme.subscribers_id_seq'::regclass),
    user_id bigint NOT NULL,
    subscriber_id bigint NOT NULL,
    CONSTRAINT subscribers_pkey PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE users_scheme.subscribers
    OWNER to student;