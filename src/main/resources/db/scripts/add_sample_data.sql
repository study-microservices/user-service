insert into users_scheme.addresses (city)
values('Минск'),
      ('Москва'),
      ('Гродно');

insert into users_scheme.skills (name)
values('Java Core'),
      ('Spring');

insert into users_scheme.users (first_name, last_name, surname, gender, birth_date, image_link, description, nikname, deleted)
values ('Иван','Иванов','Иванович','М','1985-02-10','https://dummyimage.com/165x165/c71090/fff.png','Описание пользователя','ivan_ivanov',false),
       ('Петр','Петров','Петрович','М','1990-12-15','https://dummyimage.com/165x165/c71090/fff.png','Описание пользователя','petr_petrov',false),
       ('Наталья','Никанорова','Сергеевна','Ж','1973-08-12','https://dummyimage.com/165x165/c71090/fff.png','Описание пользователя','nikanorova_natasha',false);

insert into users_scheme.user_addresses (user_id, address_id)
values (1, 1),
       (2, 1),
       (3, 2);

insert into users_scheme.user_contacts (contact_type, contact, user_id)
values (1, 'ivanov@gmail.com', 1),
       (1, 'petrov@gmail.com', 2),
       (1, 'nikanorova_natasha@gmail.com', 3),
       (2, '+375295464321', 1),
       (2, '+375332564351', 2),
       (2, '+798534565423', 3);

insert into users_scheme.user_skills (user_id, skill_id)
values (1, 1),
       (1, 2),
       (2, 1),
       (3, 1);       
