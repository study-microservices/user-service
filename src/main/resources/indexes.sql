create unique index if not exists users_fullname_unique_idx
    on users_scheme.users (last_name, first_name, surname);

create index if not exists users_lastname_idx
    on users_scheme.users (last_name);

create unique index if not exists users_nikname_unique_idx
    on users_scheme.users (nikname);

create unique index if not exists users_subscribers_unique_idx
    on users_scheme.subscribers(user_id, subscriber_id);

create unique index if not exists users_subscriptions_unique_idx
    on users_scheme.subscriptions(user_id, subscription_id);

create unique index if not exists users_address_unique_idx
    on users_scheme.user_addresses(user_id, address_id);

create unique index if not exists users_contacts_unique_idx
    on users_scheme.user_contacts(user_id, contact_type, contact);

create unique index if not exists users_skills_unique_idx
    on users_scheme.user_skills(user_id, skill_id);