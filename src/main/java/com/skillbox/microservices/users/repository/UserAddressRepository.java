package com.skillbox.microservices.users.repository;

import com.skillbox.microservices.users.entity.UserAddress;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserAddressRepository extends JpaRepository<UserAddress, Integer> {
    List<UserAddress> findAllByUserId(Long userId);

    void deleteByUserIdAndAddressId(Long userId, Integer addressId);
}