package com.skillbox.microservices.users.repository;

import com.skillbox.microservices.users.entity.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
    void deleteByUserIdAndSubscriptionId(Long userId, Long subscriptionId);
}