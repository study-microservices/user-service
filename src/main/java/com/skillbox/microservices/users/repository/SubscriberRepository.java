package com.skillbox.microservices.users.repository;

import com.skillbox.microservices.users.entity.Subscriber;
import org.springframework.data.jpa.repository.JpaRepository;


public interface SubscriberRepository extends JpaRepository<Subscriber, Long> {
    void deleteByUserIdAndSubscriberId(Long userId, Long subscriberId);

}