package com.skillbox.microservices.users.repository;

import com.skillbox.microservices.users.entity.UserContact;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserContactRepository extends JpaRepository<UserContact, Integer> {
    List<UserContact> findAllByUserId(Long userId);
}