package com.skillbox.microservices.users.repository;

import com.skillbox.microservices.users.entity.UserSkill;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserSkillRepository extends JpaRepository<UserSkill, Integer> {
    List<UserSkill> findAllByUserId(Long userId);

    void deleteByUserIdAndSkillId(Long userId, Integer skillId);
}