package com.skillbox.microservices.users.repository;

import com.skillbox.microservices.users.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Integer> {
    Address findByCity(String city);
}
