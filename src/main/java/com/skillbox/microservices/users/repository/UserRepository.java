package com.skillbox.microservices.users.repository;

import com.skillbox.microservices.users.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAllByLastName(String lastName);

    @Query(value = "select u from User u where concat(u.lastName, ' ', u.firstName, ' ', u.surName) = :fullName")
    User findByFullName(@Param("fullName") String fullName);
}
