package com.skillbox.microservices.users.repository;

import com.skillbox.microservices.users.entity.Skill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SkillRepository extends JpaRepository<Skill, Integer> {
    Skill findByName(String name);
}