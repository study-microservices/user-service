package com.skillbox.microservices.users.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    @GetMapping("/hello")
    public String handleGreeting() {
        return "Hello. User microservice greeting you.";
    }

}
