package com.skillbox.microservices.users.controller;

import com.skillbox.microservices.users.dto.SkillDto;
import com.skillbox.microservices.users.service.SkillService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/skills")
public class SkillController {
    private static final Logger logger = LoggerFactory.getLogger(SkillController.class);

    private final SkillService skillService;

    /**
     * Метод добавляет навык в базу данных
     * @param skillDto {@link SkillDto}
     * @return строка с информацией
     */
    @PostMapping
    @Operation(summary = "Добавление навыка")
    @ApiResponse(responseCode = "200", description = "Навык {name} добавлен с id = {id}")
    public String createSkill(@RequestBody SkillDto skillDto) {
        logger.info("Обработка запроса на добавление нового навыка");
        return skillService.createSkill(skillDto);
    }

    /**
     * Метод обновляет навык в базе данных
     * @param skillDto {@link SkillDto}
     * @param id {@link Integer}
     * @return строка с информацией
     */
    @PutMapping("/{id}")
    @Operation(summary = "Обновление навыка")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Навык с id = {id} успешно обновлен"),
                    @ApiResponse(responseCode = "400", description = "Навык не может быть обработан")
            }
    )
    public String updateSkill(@RequestBody SkillDto skillDto, @PathVariable Integer id) {
        logger.info("Обработка запроса на обновление навыка по идентификатору {}", id);
        // проверка согласованности данных
        if (skillDto.getId() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Идентификатор навыка не может быть пустым");
        }
        if (!skillDto.getId().equals(id)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Идентификаторы навыка не совпадают");
        }
        return skillService.updateSkill(skillDto, id);
    }

    /**
     * Метод удаляет навык из базы данных
     * @param id {@link Integer}
     * @return строка с информацией
     */
    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление пользователя")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Пользователь с id = {id} успешно удален"),
                    @ApiResponse(responseCode = "404", description = "Пользователь с id = {id} не найден")
            }
    )
    public String deleteSkill(@PathVariable Integer id) {
        logger.info("Обработка запроса на удаление навыка с id = {}", id);
        return skillService.deleteSkill(id);
    }

    /**
     * Метод возвращает список навыков
     * @return список пользователей
     */
    @GetMapping("/list")
    @Operation(summary = "Получение списка навыков")
    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = SkillDto.class)))
    public List<SkillDto> getAllSkilles() {
        logger.info("Обработка запроса на получение списка всех навыков");
        return skillService.getAllSkills();
    }
    
}
