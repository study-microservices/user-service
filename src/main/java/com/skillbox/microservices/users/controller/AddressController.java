package com.skillbox.microservices.users.controller;

import com.skillbox.microservices.users.dto.AddressDto;
import com.skillbox.microservices.users.service.AddressService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/addresses")
public class AddressController {
    private static final Logger logger = LoggerFactory.getLogger(AddressController.class);

    private final AddressService addressService;

    /**
     * Метод добавляет адрес в базу данных
     * @param addressDto {@link AddressDto}
     * @return строка с информацией
     */
    @PostMapping
    @Operation(summary = "Добавление адреса")
    @ApiResponse(responseCode = "200", description = "Адрес {city} добавлен с id = {id}")
    public String createAddress(@RequestBody AddressDto addressDto) {
        logger.info("Обработка запроса на добавление нового адреса");
        return addressService.createAddress(addressDto);
    }

    /**
     * Метод обновляет адрес в базе данных
     * @param addressDto {@link AddressDto}
     * @param id {@link Integer}
     * @return строка с информацией
     */
    @PutMapping("/{id}")
    @Operation(summary = "Обновление адреса")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Адрес с id = {id} успешно обновлен"),
                    @ApiResponse(responseCode = "400", description = "Адрес не может быть обработан")
            }
    )
    public String updateAddress(@RequestBody AddressDto addressDto, @PathVariable Integer id) {
        logger.info("Обработка запроса на обновление адреса по идентификатору {}", id);
        // проверка согласованности данных
        if (addressDto.getId() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Идентификатор адреса не может быть пустым");
        }
        if (!addressDto.getId().equals(id)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Идентификаторы адреса не совпадают");
        }
        return addressService.updateAddress(addressDto, id);
    }

    /**
     * Метод удаляет адрес из базы данных
     * @param id {@link Integer}
     * @return строка с информацией
     */
    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление пользователя")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Пользователь с id = {id} успешно удален"),
                    @ApiResponse(responseCode = "404", description = "Пользователь с id = {id} не найден")
            }
    )
    public String deleteAddress(@PathVariable Integer id) {
        logger.info("Обработка запроса на удаление адреса с id = {}", id);
        return addressService.deleteAddress(id);
    }

    /**
     * Метод возвращает список адресов
     * @return список пользователей
     */
    @GetMapping("/list")
    @Operation(summary = "Получение списка адресов")
    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = AddressDto.class)))
    public List<AddressDto> getAllAddresses() {
        logger.info("Обработка запроса на получение списка всех адресов");
        return addressService.getAllAddresses();
    }

}
