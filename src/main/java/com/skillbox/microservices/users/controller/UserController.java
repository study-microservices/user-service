package com.skillbox.microservices.users.controller;

import com.skillbox.microservices.users.dto.AddressDto;
import com.skillbox.microservices.users.dto.SkillDto;
import com.skillbox.microservices.users.dto.UserContactDto;
import com.skillbox.microservices.users.dto.UserDto;
import com.skillbox.microservices.users.service.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.security.RolesAllowed;
import java.util.List;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;
    private final UserAddressService userAddressService;
    private final UserContactService userContactService;
    private final UserSkillService userSkillService;
    private final SubscriberService subscriberService;
    private final SubscriptionService subscriptionService;

    /**
     * Метод добавляет пользователя в базу данных
     * @param userDto {@link UserDto}
     * @return строка с информацией
     */
    @PostMapping
    @Operation(summary = "Добавление пользователя")
    @ApiResponse(responseCode = "200", description = "Пользователь {firstName} {secondName} добавлен с id = {id}")
    public String createUser(@RequestBody UserDto userDto) {
        logger.info("Обработка запроса на создание пользователя {} {}", userDto.getFirstName(), userDto.getLastName());
        return userService.createUser(userDto);
    }

    /**
     * Метод возвращает пользователя по его идентификатору
     * @param id {@link Long}
     * @return пользователь {@link UserDto}
     */
    @GetMapping(path = "/{id}")
    @Operation(summary = "Получение пользователя")
    public UserDto getUser(@PathVariable Long id) {
        logger.info("Обработка запроса на получение пользователя по идентификатору {}", id);
        return userService.getUser(id);
    }

    /**
     * Метод обновляет атрибуты пользователя
     * @param userDto {@link UserDto}
     * @param id {@link Long}
     * @return строка с информацией
     */
    @PutMapping("/{id}")
    @Operation(summary = "Обновление пользователя")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Пользователь с id = {id} успешно обновлен"),
                    @ApiResponse(responseCode = "400", description = "Пользователь не может быть обработан")
            }
    )

    @RolesAllowed({ "user_admin", "user_viewer" })
    public String updateUser(@RequestBody UserDto userDto, @PathVariable Long id, @AuthenticationPrincipal Jwt jwt) {
        logger.info("Обработка запроса на обновление пользователя по идентификатору {}", id);
        // проверка согласованности данных
        if (userDto.getId() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Идентификатор пользователя не может быть пустым");
        }
        if (!userDto.getId().equals(id)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Идентификаторы пользователя не совпадают");
        }
        String clientId = getClientIdFromJwt(jwt);
        logger.info("client ID = {}", clientId);
        return userService.updateUser(userDto, id, clientId);
    }

    /**
     * Метод устанавливает пометку на удаление для пользователя
     * @param id {@link Long}
     * @return строка с информацией
     */
    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление пользователя")
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Пользователь с id = {id} успешно удален"),
                    @ApiResponse(responseCode = "404", description = "Пользователь с id = {id} не найден")
            }
    )

    @RolesAllowed({ "user_admin", "user_viewer" })
    public String deleteUser(@PathVariable Long id, @AuthenticationPrincipal Jwt jwt) {
        logger.info("Обработка запроса на удаление пользователя с id = {}", id);
        String clientId = getClientIdFromJwt(jwt);
        logger.info("client ID = {}", clientId);
        return userService.deleteUser(id, clientId);
    }

    /**
     * Метод возвращает список пользователей
     * @return список пользователей
     */
    @GetMapping("/list")
    @Parameter(in = ParameterIn.QUERY, name = "isDeleted", hidden = true)
    @Operation(summary = "Получение списка пользователей")
    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = UserDto.class)))
    public List<UserDto> getAllUsers(@RequestParam(value = "last_name", required = false) String lastName) {
        logger.info("Обработка запроса на получение списка всех пользователей");
        if (ObjectUtils.isEmpty(lastName)) {
            return userService.getAllUsers();
        } else {
            return userService.getUsersByLastName(lastName);
        }
    }

    /**
     * Метод добавляет подписчика к пользователю с id
     * @param subscriber подписчик
     * @param id идентификатор {@link Long}
     * @return строка с информацией
     */
    @PostMapping(value = "/{id}/subscribe")
    @Operation(summary = "Добавление подписчика")
    @ApiResponse(responseCode = "200", description = "Пользователь {firstName} {secondName} подписался на пользователя с id = {id}")
    public String subscribeToUser(@RequestBody UserDto subscriber, @PathVariable Long id) {
        logger.info("Обработка запроса на добавление подписчика для пользователя");
        return subscriberService.subscribe(subscriber, id);
    }

    /**
     * Метод удаляет подписчика у пользователя с id
     * @param subscriber подписчик
     * @param id идентификатор {@link Long}
     * @return строка с информацией
     */
    @PostMapping(value = "/{id}/unsubscribe")
    @Operation(summary = "Удаление подписчика")
    @ApiResponse(responseCode = "200", description = "Пользователь {firstName} {secondName} отписался от пользователя с id = {id}")
    public String unsubscribeFromUser(@RequestBody UserDto subscriber, @PathVariable Long id) {
        logger.info("Обработка запроса на удаление подписчика от пользователя");
        return subscriberService.unsubscribe(subscriber, id);
    }

    /**
     * Метод добавляет подписку на пользователя с id
     * @param subscription подписчик
     * @param id идентификатор {@link Long}
     * @return строка с информацией
     */
    @PostMapping(value = "/{id}/subscription")
    @Operation(summary = "Добавление подписки")
    @ApiResponse(responseCode = "200", description = "Пользователь {firstName} {secondName} подписался на пользователя с id = {id}")
    public String subscriptionToUser(@RequestBody UserDto subscription, @PathVariable Long id) {
        logger.info("Обработка запроса на добавление подписки для пользователя");
        return subscriptionService.subscription(subscription, id);
    }

    /**
     * Метод удаляет подписчика у пользователя с id
     * @param subscription подписант
     * @param id идентификатор {@link Long}
     * @return строка с информацией
     */
    @PostMapping(value = "/{id}/unsubscription")
    @Operation(summary = "Удаление подписки")
    @ApiResponse(responseCode = "200", description = "Пользователь {firstName} {secondName} отписался от пользователя с id = {id}")
    public String unsubscriptionFromUser(@RequestBody UserDto subscription, @PathVariable Long id) {
        logger.info("Обработка запроса на удаление подписки от пользователя");
        return subscriptionService.unsubscription(subscription, id);
    }

    /**
     * Метод возвращает список адресов пользователя
     * @param userId идентификатор пользователя {@link Long}
     * @return {@link List}
     */
    @GetMapping(value = "/{id}/address/list")
    @Operation(summary = "Получение списка адресов пользователя")
    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = UserDto.class)))
    public List<AddressDto> getUserAddress(@PathVariable("id") Long userId) {
        logger.info("Обработка запроса на получение списка адресов пользователя");
        return userAddressService.getUserAddresses(userId);
    }

    /**
     * Метод добавляет адрес пользователю
     * @param addressDto адрес {@link AddressDto}
     * @return строка с описанием
     */
    @PostMapping(value = "/{id}/address/add")
    @Operation(summary = "Добавление адреса")
    @ApiResponse(responseCode = "200", description = "Добавлен адрес пользователю с id = {id}")
    public String createUserAddress(@RequestBody AddressDto addressDto, @PathVariable("id") Long userId) {
        logger.info("Обработка запроса на добавление адреса пользователю");
        return userAddressService.createUserAddress(addressDto, userId);
    }

    /**
     * Метод удаляет адрес у пользователя
     * @param userId идентификатор пользователя
     * @param addressId идентификатор адреса
     * @return строка с информацией
     */
    @PostMapping(value = "/{id}/address/{addressId}/delete")
    @Operation(summary = "Удаление адреса")
    @ApiResponse(responseCode = "200", description = "Удален адрес у пользователя с id = {id}")
    public String deleteUserAddress(@PathVariable("id") Long userId, @PathVariable("addressId") Integer addressId) {
        logger.info("Обработка запроса на удаление адреса у пользователя");
        return userAddressService.deleteUserAddress(userId, addressId);
    }

    /**
     * Метод возвращает список контактов пользователя
     * @param userId идентификатор пользователя {@link Long}
     * @return {@link List}
     */
    @GetMapping(value = "/{id}/contacts/list")
    @Operation(summary = "Получение списка контактов пользователя")
    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = UserDto.class)))
    public List<UserContactDto> getUserContacts(@PathVariable("id") Long userId) {
        logger.info("Обработка запроса на получение списка адресов пользователя");
        return userContactService.getUserContacts(userId);
    }

    /**
     * Метод добавляет контакт пользователю
     * @param userContactDto контакт {@link UserContactDto}
     * @return строка с описанием
     */
    @PostMapping(value = "/{id}/contact/add")
    @Operation(summary = "Добавление контакта")
    @ApiResponse(responseCode = "200", description = "Добавлен контакт пользователю с id = {id}")
    public String createUserAddress(@RequestBody UserContactDto userContactDto, @PathVariable("id") Long userId) {
        logger.info("Обработка запроса на добавление контакта пользователю");
        return userContactService.createUserContact(userContactDto, userId);
    }

    /**
     * Метод удаляет адрес у пользователя
     * @param userId идентификатор пользователя
     * @param contactId идентификатор контакта
     * @return строка с информацией
     */
    @PostMapping(value = "/{id}/contact/{contactId}/delete")
    @Operation(summary = "Удаление контакта")
    @ApiResponse(responseCode = "200", description = "Удален контакт у пользователя с id = {id}")
    public String deleteUserContact(@PathVariable("id") Long userId, @PathVariable("contactId") Integer contactId) {
        logger.info("Обработка запроса на удаление адреса у пользователя");
        return userContactService.deleteUserContact(userId, contactId);
    }

    /**
     * Метод возвращает список навыков пользователя
     * @param userId идентификатор пользователя {@link Long}
     * @return {@link List}
     */
    @GetMapping(value = "/{id}/skills/list")
    @Operation(summary = "Получение списка навыков пользователя")
    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = SkillDto.class)))
    public List<SkillDto> getUserSkills(@PathVariable("id") Long userId) {
        logger.info("Обработка запроса на получение списка навыков пользователя");
        return userSkillService.getUserSkills(userId);
    }

    /**
     * Метод добавляет навык пользователю
     * @param skillDto адрес {@link SkillDto}
     * @return строка с описанием
     */
    @PostMapping(value = "/{id}/skills/add")
    @Operation(summary = "Добавление навыка")
    @ApiResponse(responseCode = "200", description = "Добавлен навык пользователю с id = {id}")
    public String createUserSkill(@RequestBody SkillDto skillDto, @PathVariable("id") Long userId) {
        logger.info("Обработка запроса на добавление навыка пользователю");
        return userSkillService.createUserSkill(skillDto, userId);
    }

    /**
     * Метод удаляет навык у пользователя
     * @param userId идентификатор пользователя
     * @param skillId идентификатор навыка
     * @return строка с информацией
     */
    @PostMapping(value = "/{id}/skills/{skillId}/delete")
    @Operation(summary = "Удаление навыка")
    @ApiResponse(responseCode = "200", description = "Удален навык у пользователя с id = {id}")
    public String deleteUserSkill(@PathVariable("id") Long userId, @PathVariable("skillId") Integer skillId) {
        logger.info("Обработка запроса на удаление навык у пользователя");
        return userSkillService.deleteUserSkill(userId, skillId);
    }

    private String getClientIdFromJwt(Jwt jwt) {
        Object clientId = jwt.getClaims().get("preferred_username");
        Objects.requireNonNull(clientId, "Client ID is missing in JWT");
        return (String) clientId;
    }

    @SuppressWarnings("unchecked")
    private boolean isAdmin(Jwt jwt) {
        List<String> roles = (List<String>) jwt.getClaimAsMap("realm_access").get("roles");
        return roles.stream().anyMatch(role -> role.equals("user_admin"));
    }

}
