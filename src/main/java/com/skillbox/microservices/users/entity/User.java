package com.skillbox.microservices.users.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.skillbox.microservices.users.enums.Gender;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Builder
@Getter
@Setter
@Entity
@Table(name = "users", schema = "users_scheme")
@NoArgsConstructor
@AllArgsConstructor
@SQLDelete(sql = "UPDATE users_scheme.users SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class User {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "Уникальный идентификатор пользователя", example = "1")
    private Long id;

    @Column(name = "first_name", nullable = false, length = 30)
    @Schema(description = "Имя", example = "Иван")
    private String firstName;

    @Column(name = "last_name", nullable = false, length = 30)
    @Schema(description = "Фамилия", example = "Иванов")
    private String lastName;

    @Column(name = "surname", length = 30)
    @Schema(description = "Отчество", example = "Иванович")
    private String surName;

    @Column(name = "birth_date")
    @Schema(description = "Дата рождения", example = "Иван")
    @JsonFormat(pattern = "dd.MM.yyyy")
    private LocalDate birthDate;

    @Column(name = "image_link")
    @Schema(description = "Ссылка на аватар", example = "https://dummyimage.com/165x165/c71090/fff.png")
    private String imageLink;

    @Column(name = "description")
    @Type(type = "org.hibernate.type.TextType")
    @Schema(description = "Описание", example = "Описание пользователя")
    private String description;

    @Column(name = "nikname", nullable = false, length = 30)
    @Schema(description = "Ник пользователя", example = "ivan_ivanov")
    private String nikname;

    @Schema(description = "Пол", implementation = Gender.class)
    private Gender gender;

    @Column(name = "deleted", nullable = false)
    @Schema(hidden = true)
    @Builder.Default
    private Boolean deleted = Boolean.FALSE;

    @OneToMany(mappedBy = "user")
    @JsonIgnore
    @Builder.Default
    private Set<Subscriber> subscribers = new HashSet<>();

    @OneToMany(mappedBy = "user")
    @JsonIgnore
    @Builder.Default
    private Set<Subscription> subscriptions = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        User user = (User) o;
        return getId() != null && Objects.equals(getId(), user.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public String getFullName() {
        return String.join(" ", this.getLastName(), this.getFirstName(), this.getSurName());
    }
}