package com.skillbox.microservices.users.converter;

import com.skillbox.microservices.users.enums.Gender;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class GenderConverter implements AttributeConverter<Gender, String> {
    @Override
    public String convertToDatabaseColumn(Gender gender) {
        if (gender == null) {
            return null;
        }
        return gender.getName();
    }

    @Override
    public Gender convertToEntityAttribute(String gender) {
        if (gender == null) {
            return null;
        }
        return Gender.valueByName(gender);
    }

}