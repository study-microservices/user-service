package com.skillbox.microservices.users.enums;

public enum ContactType {
    EMAIL,
    PHONE
}