package com.skillbox.microservices.users.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum Gender {

    MALE("М"),
    FEMALE("Ж");
    public final String name;

    Gender(String name) {
        this.name = name;
    }

    @JsonValue
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return String.valueOf(name);
    }

    private static final Map<String, Gender> BY_NAME = new HashMap<>();

    static {
        for (Gender e: values()) {
            BY_NAME.put(e.name, e);
        }
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static Gender valueByName(String name) {
        return BY_NAME.get(name);
    }

}