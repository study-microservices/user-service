package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.dto.SkillDto;
import com.skillbox.microservices.users.entity.Skill;
import com.skillbox.microservices.users.entity.User;
import com.skillbox.microservices.users.entity.UserSkill;
import com.skillbox.microservices.users.repository.UserSkillRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class UserSkillService {

    private final UserSkillRepository userSkillRepository;
    private final UserService userService;
    private final SkillService skillService;
    private final ModelMapper modelMapper;

    public String createUserSkill(SkillDto skillDto, Long userId) {
        User user = userService.getUserById(userId);
        Integer skillId = skillDto.getId();
        Skill skill = skillService.getSkillById(skillId);
        UserSkill userSkill = UserSkill.builder()
                .user(user)
                .skill(skill)
                .build();
        userSkillRepository.save(userSkill);
        return String.format("Пользователю с id = %s добавлен навык с id = %s",
                userId, skillId);
    }

    public List<SkillDto> getUserSkills(Long userId) {
        userService.checkUser(userId);
        List<UserSkill> userSkills = userSkillRepository.findAllByUserId(userId);
        List<Skill> skills = userSkills.stream()
                .map(UserSkill::getSkill)
                .collect(Collectors.toList());
        return skills.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    private SkillDto convertToDto(Skill skill) {
        return modelMapper.map(skill, SkillDto.class);
    }

    public String deleteUserSkill(Long userId, Integer skillId) {
        userSkillRepository.deleteByUserIdAndSkillId(userId, skillId);
        return String.format("У пользователя с id = %s удален навык с id = %s",
                userId, skillId);
    }
}
