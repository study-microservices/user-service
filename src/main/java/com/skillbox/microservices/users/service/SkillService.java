package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.dto.SkillDto;
import com.skillbox.microservices.users.entity.Skill;
import com.skillbox.microservices.users.repository.SkillRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SkillService {

    private final SkillRepository skillRepository;

    private final ModelMapper modelMapper;

    public String createSkill(SkillDto skillDto) {
        Skill skill = convertToEntity(skillDto);
        // проверка существования
        if (skillRepository.findByName(skill.getName()) != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Навык %s уже существует", skill.getName()));
        }
        Skill saved = skillRepository.save(skill);
        return String.format("Добавлен навык %s с id = %s", saved.getName(), saved.getId());
    }

    public String updateSkill(SkillDto skillDto, Integer id) {
        Skill skill = convertToEntity(skillDto);
        checkSkill(id);
        Skill saved = skillRepository.save(skill);
        return String.format("Навык с id = %s успешно обновлен", saved.getId());
    }

    public String deleteSkill(Integer id) {
        checkSkill(id);
        skillRepository.deleteById(id);
        return String.format("Навык с id = %s успешно удален", id);
    }

    private void checkSkill(Integer id) {
        if (!skillRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Навык с id = %s не найден", id));
        }
    }

    public List<SkillDto> getAllSkills() {
        List<Skill> skills = skillRepository.findAll();
        return skills.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    /**
     * Метод конвертирует entity в dto
     * @param skill адрес {@link Skill}
     * @return {@link SkillDto}
     */
    private SkillDto convertToDto(Skill skill) {
        return modelMapper.map(skill, SkillDto.class);
    }

    /**
     * Метод конвертирует dto в entity
     * @param skillDto {@link SkillDto}
     * @return {@link Skill}
     */
    private Skill convertToEntity(SkillDto skillDto) {
        return modelMapper.map(skillDto, Skill.class);
    }

    public Skill getSkillById(Integer skillId) {
        return skillRepository.findById(skillId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Навык с идентификатором %s не найден", skillId))
        );
    }
}
