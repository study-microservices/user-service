package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.dto.UserDto;
import com.skillbox.microservices.users.entity.Subscription;
import com.skillbox.microservices.users.entity.User;
import com.skillbox.microservices.users.repository.SubscriptionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class SubscriptionService {

    private final SubscriptionRepository subscriptionRepository;
    private final UserService userService;

    public String subscription(UserDto userDto, Long userSubscriptionId) {
        User user = userService.getUserById(userDto.getId());
        User userSubscription = userService.getUserById(userSubscriptionId);
        Subscription subscriber = Subscription
                .builder()
                .user(user)
                .subscription(userSubscription)
                .build();
        subscriptionRepository.save(subscriber);
        return String.format("Пользователь %s подписался на пользователя %s",
                userSubscription.getFullName(),
                user.getFullName());
    }

    public String unsubscription(UserDto userDto, Long userSubscriptionId) {
        User user = userService.getUserById(userDto.getId());
        User userSubscription = userService.getUserById(userSubscriptionId);
        subscriptionRepository.deleteByUserIdAndSubscriptionId(user.getId(), userSubscriptionId);
        return String.format("Пользователь %s отписался от пользователя с id = %s",
                userSubscription.getFullName(), user.getId());
    }

}
