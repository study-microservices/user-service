package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.dto.AddressDto;
import com.skillbox.microservices.users.entity.Address;
import com.skillbox.microservices.users.repository.AddressRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AddressService {

    private final AddressRepository addressRepository;

    private final ModelMapper modelMapper;

    public String createAddress(AddressDto addressDto) {
        Address address = convertToEntity(addressDto);
        // проверка существования
        if (addressRepository.findByCity(address.getCity()) != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Адрес %s уже существует", address.getCity()));
        }
        Address saved = addressRepository.save(address);
        return String.format("Добавлен адрес %s с id = %s", saved.getCity(), saved.getId());
    }

    public String updateAddress(AddressDto addressDto, Integer id) {
        Address address = convertToEntity(addressDto);
        checkAddress(id);
        Address saved = addressRepository.save(address);
        return String.format("Адрес с id = %s успешно обновлен", saved.getId());
    }

    public String deleteAddress(Integer id) {
        checkAddress(id);
        addressRepository.deleteById(id);
        return String.format("Адрес с id = %s успешно удален", id);
    }

    public Address getAddressById(Integer id) {
        return addressRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                String.format("Адрес с id = %s не найден", id)));
    }

    private void checkAddress(Integer id) {
        if (!addressRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Адрес с id = %s не найден", id));
        }
    }

    public List<AddressDto> getAllAddresses() {
        List<Address> addresses = addressRepository.findAll();
        return addresses.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    /**
     * Метод конвертирует entity в dto
     * @param address адрес {@link Address}
     * @return {@link AddressDto}
     */
    private AddressDto convertToDto(Address address) {
        return modelMapper.map(address, AddressDto.class);
    }

    /**
     * Метод конвертирует dto в entity
     * @param addressDto {@link AddressDto}
     * @return {@link Address}
     */
    private Address convertToEntity(AddressDto addressDto) {
        return modelMapper.map(addressDto, Address.class);
    }

}
