package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.dto.UserDto;
import com.skillbox.microservices.users.entity.Subscriber;
import com.skillbox.microservices.users.entity.User;
import com.skillbox.microservices.users.repository.SubscriberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class SubscriberService {

    private final SubscriberRepository subscriberRepository;
    private final UserService userService;

    public String subscribe(UserDto userDto, Long userId) {
        User user = userService.getUserById(userId);
        User userSubscriber = userService.getUserById(userDto.getId());
        Subscriber subscriber = Subscriber
                .builder()
                .user(user)
                .subscriber(userSubscriber)
                .build();
        subscriberRepository.save(subscriber);
        return String.format("Пользователь %s подписался на пользователя с id = %s",
                userSubscriber.getFullName(), user.getId());
    }

    public String unsubscribe(UserDto userDto, Long userId) {
        User user = userService.getUserById(userId);
        User userSubscriber = userService.getUserById(userDto.getId());
        subscriberRepository.deleteByUserIdAndSubscriberId(userId, user.getId());
        return String.format("Пользователь %s отписался от пользователя с id = %s",
                userSubscriber.getFullName(), user.getId());
    }
}
