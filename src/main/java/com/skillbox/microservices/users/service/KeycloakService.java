package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.Credentials;
import com.skillbox.microservices.users.dto.UserDto;
import liquibase.repackaged.org.apache.commons.lang3.exception.ExceptionUtils;
import lombok.RequiredArgsConstructor;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.util.Collections;

@RequiredArgsConstructor
@Service
public class KeycloakService {
    private static final Logger logger = LoggerFactory.getLogger(KeycloakService.class);

    @Value("${keycloak.realm}")
    private String realm;
    @Value("${keycloak.resource}")
    private String clientId;

    private final Keycloak keycloak;

    /**
     * Create new user in keycloak
     * @param userDTO
     */
    public void addUser(UserDto userDTO) {
        UserRepresentation user = getKeycloakUser(userDTO);
        user.setEnabled(true);
        UsersResource userResource = getUserResource();
        try {
            // Create user (requires manage-users role)
            Response response = userResource.create(user);
            logger.info("Response: {} {}", response.getStatus(), response.getStatusInfo());
            logger.info("Location: {}", response.getLocation());
            String userId = CreatedResponseUtil.getCreatedId(response);
            logger.info("User created with userId: {}", userId);
            // define user role
            addRealmRoleToUser(userId, "user_viewer");
        } catch (Exception ex) {
            logger.error(ExceptionUtils.getStackTrace(ex));
            throw new RuntimeException(ex);
        }

    }

    /**
     * Update user information in keycloak
     * @param userId
     * @param userDTO
     */
    public void updateUser(String userId, UserDto userDTO){
        UserRepresentation user = getKeycloakUser(userDTO);
        UsersResource usersResource = getUserResource();
        usersResource.get(userId).update(user);
    }

    private UserRepresentation getKeycloakUser(UserDto userDTO) {
        CredentialRepresentation credential = Credentials
                .createPasswordCredentials(userDTO.getPassword());
        UserRepresentation user = new UserRepresentation();
        user.setEnabled(true);
        user.setUsername(userDTO.getNikname());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
//        user.setEmail(userDTO.getEmailId());
        user.setCredentials(Collections.singletonList(credential));
        return user;
    }

    private UsersResource getUserResource(){
        return keycloak
                .realm(realm)
                .users();
    }

    private void addRealmRoleToUser(String userId, String roleName) {
        RealmResource realmResource = keycloak
                .realm(realm);
        UserResource userResource = getUserResource()
                .get(userId);

        // Get realm role "tester" (requires view-realm role)
        RoleRepresentation realmRole = realmResource.roles()//
                .get(roleName).toRepresentation();

        // Assign realm role tester to user
        userResource
                .roles()
                .realmLevel()
                .add(Collections.singletonList(realmRole));

        // Get client
        ClientRepresentation app1Client = realmResource
                .clients()
                .findByClientId(clientId)
                .get(0);

        // Get client level role (requires view-clients role)
        RoleRepresentation userClientRole = realmResource
                .clients()
                .get(app1Client.getId())
                .roles()
                .get(roleName)
                .toRepresentation();

        // Assign client level role to user
        userResource
                .roles() //
                .clientLevel(app1Client.getId())
                .add(Collections.singletonList(userClientRole));

    }

}