package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.dto.UserDto;
import com.skillbox.microservices.users.entity.User;
import com.skillbox.microservices.users.enums.Gender;
import com.skillbox.microservices.users.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final KeycloakService keycloakService;

    public List<UserDto> getUsersByLastName(String secondName) {
        List<User> users = userRepository.findAllByLastName(secondName);
        return users.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    public List<UserDto> getAllUsers() {
        List<User> users = userRepository.findAll();
        return users.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public String createUser(UserDto userDto) {
        User user = convertToEntity(userDto);
        if (userExists(user)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Пользователь %s уже существует", user.getFullName()));
        }
        try {
            User savedUser = userRepository.save(user);

            // регистрация юзера в keycloak
            keycloakService.addUser(userDto);

            return String.format("Пользователь %s добавлен с id = %s",
                    savedUser.getFullName(), savedUser.getId());
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Не удалось добавить пользователя\n\rОшибка: %s", ex.getMessage()));
        }
    }

    private boolean userExists(User user) {
        User foundUser = userRepository.findByFullName(user.getFullName());
        return foundUser != null;
    }

    public UserDto getUser(Long id) {
        return convertToDto(getUserById(id));
    }

    public User getUserById(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                String.format("Пользователь с id = %s не найден", id)));
    }

    public String updateUser(UserDto userDto, Long id, String clientId) {
        User user = convertToEntity(userDto);
        // check user permissions
        if (user.getNikname().equals(clientId)) {
            checkUser(id);
            try {
                User savedUser = userRepository.save(user);
                return String.format("Пользователь с id = %s успешно обновлен", savedUser.getId());
            } catch (Exception ex) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Не удалось обновить атрибуты пользователя с id = %s", id));
            }
        } else {
            throw new AccessDeniedException(String.format("У пользователя %s недостаточно прав для выполнения операции", clientId));
        }

    }

    public void checkUser(Long id) {
        if (!userRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Пользователь с id = %s не найден", id));
        }
    }

    public String deleteUser(Long id, String clientId) {
        User user = getUserById(id);
        // check user permissions
        if (user.getNikname().equals(clientId)) {
            try {
                userRepository.deleteById(id);
                return String.format("Пользователь с id = %s успешно удален", id);
            } catch (Exception ex) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Не удалось удалить пользователя с id = %s", id));
            }
        } else {
            throw new AccessDeniedException(String.format("У пользователя %s недостаточно прав для выполнения операции", clientId));
        }
    }

    /**
     * Метод конвертирует entity в dto
     *
     * @param user пользователь {@link User}
     * @return {@link UserDto}
     */
    UserDto convertToDto(User user) {
        return modelMapper.map(user, UserDto.class);
    }

    /**
     * Метод конвертирует dto в entity
     *
     * @param userDto {@link UserDto}
     * @return {@link User}
     */
    User convertToEntity(UserDto userDto) {
        User user = modelMapper.map(userDto, User.class);
        user.setGender(Gender.valueByName(userDto.getGender()));
        return user;
    }

}
