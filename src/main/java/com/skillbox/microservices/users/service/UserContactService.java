package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.dto.UserContactDto;
import com.skillbox.microservices.users.entity.User;
import com.skillbox.microservices.users.entity.UserContact;
import com.skillbox.microservices.users.enums.ContactType;
import com.skillbox.microservices.users.repository.UserContactRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class UserContactService {

    private final UserContactRepository userContactRepository;
    private final UserService userService;
    private final ModelMapper modelMapper;

    public List<UserContactDto> getUserContacts(Long userId) {
        userService.checkUser(userId);
        List<UserContact> contacts = userContactRepository.findAllByUserId(userId);
        return contacts.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    UserContactDto convertToDto(UserContact userContact) {
        return modelMapper.map(userContact, UserContactDto.class);
    }

    public String createUserContact(UserContactDto userContactDto, Long userId) {
        User user = userService.getUserById(userId);
        UserContact userContact = UserContact.builder()
                .contact(userContactDto.getContact())
                .contactType(ContactType.valueOf(userContactDto.getContactType()))
                .user(user)
                .build();
        UserContact saved = userContactRepository.save(userContact);
        return String.format("Пользователю с id = %s добавлен контакт с id = %s",
                userId, saved.getId());
    }

    public String deleteUserContact(Long userId, Integer contactId) {
        userService.checkUser(userId);
        try {
            userContactRepository.deleteById(contactId);
            return String.format("У пользователя с id = %s удален контакт с id = %s",
                    userId, contactId);
        } catch(Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Не удалось удалить контакт у пользователя с id = %s", userId));
        }
    }
}
