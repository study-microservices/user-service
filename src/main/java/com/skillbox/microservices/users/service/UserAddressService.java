package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.dto.AddressDto;
import com.skillbox.microservices.users.entity.Address;
import com.skillbox.microservices.users.entity.User;
import com.skillbox.microservices.users.entity.UserAddress;
import com.skillbox.microservices.users.repository.UserAddressRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class UserAddressService {

    private final UserAddressRepository userAddressRepository;
    private final UserService userService;
    private final AddressService addressService;
    private final ModelMapper modelMapper;

    public String createUserAddress(AddressDto addressDto, Long userId) {
        User user = userService.getUserById(userId);
        Integer addressId = addressDto.getId();
        Address address = addressService.getAddressById(addressId);
        UserAddress userAddress = UserAddress.builder()
                .user(user)
                .address(address)
                .build();
        userAddressRepository.save(userAddress);
        return String.format("Пользователю с id = %s добавлен адрес с id = %s",
                userId, addressId);
    }

    public List<AddressDto> getUserAddresses(Long userId) {
        List<UserAddress> userAddressList = userAddressRepository.findAllByUserId(userId);
        List<Address> addressList = userAddressList.stream()
                .map(UserAddress::getAddress)
                .collect(Collectors.toList());
        return addressList.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    private AddressDto convertToDto(Address address) {
        return modelMapper.map(address, AddressDto.class);
    }

    public String deleteUserAddress(Long userId, Integer addressId) {
        userAddressRepository.deleteByUserIdAndAddressId(userId, addressId);
        return String.format("У пользователя с id = %s удален адрес с id = %s",
                userId, addressId);
    }

}
