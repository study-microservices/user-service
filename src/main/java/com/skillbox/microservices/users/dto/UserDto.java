package com.skillbox.microservices.users.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.time.LocalDate;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String surName;
    @JsonFormat(pattern = "dd.MM.yyyy")
    private LocalDate birthDate;
    private String imageLink;
    private String description;
    private String nikname;
    private String password;
    private String gender;
}
