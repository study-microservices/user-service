package com.skillbox.microservices.users.dto;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserContactDto {

    private Integer id;
    private String contact;
    private String contactType;

}
