package com.skillbox.microservices.users.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AddressDto {

    private Integer id;
    private String city;

}
