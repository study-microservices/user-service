package com.skillbox.microservices.users.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SkillDto {
    private Integer id;
    private String name;
}
