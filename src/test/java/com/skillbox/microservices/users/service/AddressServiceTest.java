package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.dto.AddressDto;
import com.skillbox.microservices.users.entity.Address;
import com.skillbox.microservices.users.repository.AddressRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class AddressServiceTest extends AbstractService {

    private final AddressRepository addressRepository = mock(AddressRepository.class);
    private final AddressService addressService = new AddressService(addressRepository, modelMapper);

    @Test
    void createAddressSuccess() {
        String city = "Минск";
        AddressDto addressDto = AddressDto.builder()
                .city(city)
                .build();
        Address address = Address.builder()
                .id(1)
                .city(city)
                .build();
        when(addressRepository.findByCity(city)).thenReturn(null);
        when(addressRepository.save(any(Address.class))).thenReturn(address);
        String result = addressService.createAddress(addressDto);
        String expected = "Добавлен адрес Минск с id = 1";
        assertEquals(expected, result);
    }

    @Test
    void createAddressIfExist() {
        String city = "Минск";
        AddressDto addressDto = AddressDto.builder()
                .city(city)
                .build();
        Address address = Address.builder()
                .city(city)
                .build();
        when(addressRepository.findByCity(city)).thenReturn(address);
        Executable executable = () -> addressService.createAddress(addressDto);
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, executable);
        String expected = String.format("400 BAD_REQUEST \"%s\"", "Адрес Минск уже существует");
        String actual = String.format("%s", exception.getMessage());
        assertEquals(expected, actual);
    }

    @Test
    void updateAddress() {
        String city = "Минск";
        AddressDto addressDto = AddressDto.builder()
                .id(1)
                .city(city)
                .build();
        Address address = convert(addressDto, Address.class);
        when(addressRepository.existsById(1)).thenReturn(true);
        when(addressRepository.save(any(Address.class))).thenReturn(address);
        String result = addressService.updateAddress(addressDto, 1);
        String expected = String.format("Адрес с id = %s успешно обновлен", address.getId());
        assertEquals(expected, result);
    }

    @Test
    void deleteAddress() {
        when(addressRepository.existsById(1)).thenReturn(true);
        doNothing().when(addressRepository).deleteById(1);
        String result = addressService.deleteAddress(1);
        String expected = "Адрес с id = 1 успешно удален";
        assertEquals(expected, result);
    }

    @Test
    void getAddressById() {
        Address address = Address.builder()
                .id(1)
                .city("Минск")
                .build();
        when(addressRepository.findById(1)).thenReturn(Optional.of(address));
        Address actual = addressService.getAddressById(1);
        assertEquals(address, actual);
    }

    @Test
    void getAddressByIdNotFound() {
        when(addressRepository.findById(1)).thenReturn(Optional.ofNullable(null));
        Executable executable = () -> addressService.getAddressById(1);
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, executable);
        String expected = String.format("404 NOT_FOUND \"%s\"", "Адрес с id = 1 не найден");
        String actual = String.format("%s", exception.getMessage());
        assertEquals(expected, actual);
    }

    @Test
    void getAllAddresses() {
        Address address = Address.builder()
                .id(1)
                .city("Минск")
                .build();
        List<Address> addresses = Collections.singletonList(address);
        List<AddressDto> addressDtos = addresses.stream()
                .map(a -> convert(a, AddressDto.class))
                .collect(Collectors.toList());
        when(addressRepository.findAll()).thenReturn(addresses);
        List<AddressDto> result = addressService.getAllAddresses();
        assertEquals(addressDtos.size(), result.size());
    }
}