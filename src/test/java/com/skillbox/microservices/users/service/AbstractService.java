package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.entity.User;
import org.modelmapper.ModelMapper;

import java.time.LocalDate;

public class AbstractService {

    protected ModelMapper modelMapper = new ModelMapper();

    protected User createTestUser() {
        return User.builder()
                .id(1L)
                .firstName("Иван")
                .lastName("Иванов")
                .surName("Иванович")
                .birthDate(LocalDate.of(1995, 2, 10))
                .nikname("ivan_ivanov")
                .description("Описание пользователя")
                .build();
    }

    protected <T, R> R convert(T t, Class<R> clazz ) {
        return modelMapper.map(t, clazz);
    }

}
