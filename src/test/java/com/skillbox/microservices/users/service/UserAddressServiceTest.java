package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.dto.AddressDto;
import com.skillbox.microservices.users.entity.Address;
import com.skillbox.microservices.users.entity.User;
import com.skillbox.microservices.users.entity.UserAddress;
import com.skillbox.microservices.users.repository.UserAddressRepository;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class UserAddressServiceTest extends AbstractService {

    private final UserAddressRepository userAddressRepository = mock(UserAddressRepository.class);
    private final UserService userService = mock(UserService.class);
    private final AddressService addressService = mock(AddressService.class);
    private final UserAddressService userAddressService = new UserAddressService(userAddressRepository,
            userService, addressService, modelMapper);

    @Test
    void createUserAddress() {
        User user = createTestUser();
        Address address = Address.builder()
                .id(1)
                .city("Минск")
                .build();
        AddressDto addressDto = convert(address, AddressDto.class);
        when(userService.getUserById(1L)).thenReturn(user);
        when(addressService.getAddressById(1)).thenReturn(address);
        UserAddress userAddress = UserAddress.builder()
                .user(user)
                .address(address)
                .build();
        when(userAddressRepository.save(any(UserAddress.class))).thenReturn(userAddress);
        String result = userAddressService.createUserAddress(addressDto, 1L);
        String expected = "Пользователю с id = 1 добавлен адрес с id = 1";
        assertEquals(expected, result);
    }

    @Test
    void getUserAddresses() {
        Address address = Address.builder()
                .id(1)
                .city("Минск")
                .build();
        UserAddress userAddress = UserAddress.builder()
                .user(createTestUser())
                .address(address)
                .build();
        List<UserAddress> addresses = Collections.singletonList(userAddress);
        doNothing().when(userService).checkUser(1L);
        when(userAddressRepository.findAllByUserId(1L)).thenReturn(addresses);
        List<AddressDto> addressDtos = addresses
                .stream()
                .map((uc) -> modelMapper.map(uc, AddressDto.class))
                .collect(Collectors.toList());
        assertFalse(addressDtos.isEmpty());
    }

    @Test
    void deleteUserAddress() {
        doNothing().when(userService).checkUser(1L);
        doNothing().when(userAddressRepository).deleteById(1);
        String result = userAddressService.deleteUserAddress(1L, 1);
        String expected = "У пользователя с id = 1 удален адрес с id = 1";
        assertEquals(expected, result);
    }
}