package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.dto.UserDto;
import com.skillbox.microservices.users.entity.User;
import com.skillbox.microservices.users.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class UserServiceTest extends AbstractService {

    private final UserRepository userRepository = Mockito.mock(UserRepository.class);
    private final KeycloakService keycloakService = Mockito.mock(KeycloakService.class);
    UserService userService = new UserService(userRepository, modelMapper, keycloakService);

    @Test
    void getUsersByLastNameAndReturnEmptyList() {
        String searchName = "Неизвестный";
        when(userRepository.findAllByLastName(searchName)).thenReturn(new ArrayList<>());
        List<UserDto> result = userService.getUsersByLastName(searchName);
        assertTrue(result.isEmpty());
    }

    @Test
    void getUsersByLastNameAndReturnList() {
        String searchName = "Иванов";
        User user = createTestUser();
        List<User> users = new ArrayList<>();
        users.add(user);
        when(userRepository.findAllByLastName(searchName)).thenReturn(users);
        List<UserDto> result = userService.getUsersByLastName(searchName);
        assertFalse(result.isEmpty());
    }

    @Test
    void createUserSuccess() {
        User user = User.builder()
                .firstName("Иван")
                .lastName("Иванов")
                .surName("Иванович")
                .birthDate(LocalDate.of(1995, 2, 10))
                .nikname("ivan_ivanov")
                .description("Описание пользователя")
                .build();
        User saved = createTestUser();
        when(userRepository.save(any(User.class))).thenReturn(saved);
        UserDto userDto = userService.convertToDto(user);
        doNothing().when(keycloakService).addUser(userDto);

        String result = userService.createUser(userDto);
        String expected = "Пользователь Иванов Иван Иванович добавлен с id = 1";
        assertEquals(expected, result);
    }

    @Test
    void createUserIfExist() {
        User user = createTestUser();
        String fullName = "Иванов Иван Иванович";
        when(userRepository.findByFullName(fullName)).thenReturn(user);
        UserDto userDto = userService.convertToDto(user);
        Executable executable = () -> userService.createUser(userDto);
        assertThrows(ResponseStatusException.class, executable);
    }

    @Test
    void getUserSuccess() {
        User user = createTestUser();
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        UserDto userDto = userService.getUser(1L);
        assertInstanceOf(UserDto.class, userDto);
    }

    @Test
    void getUserFail() {
        when(userRepository.findById(2L)).thenThrow(ResponseStatusException.class);
        Executable executable = () -> userService.getUser(1L);
        assertThrows(ResponseStatusException.class, executable);
    }

    @Test
    void updateUserSuccess() {
        UserDto userDto = createTestUserDto();
        when(userRepository.existsById(1L)).thenReturn(true);
        User savedUser = createTestUser();
        when(userRepository.save(any(User.class))).thenReturn(savedUser);
        String result = userService.updateUser(userDto, 1L, "ivan_ivanov");
        String expected = "Пользователь с id = 1 успешно обновлен";
        assertEquals(expected, result);
    }

    @Test
    void updateUserFail() {
        UserDto userDto = createTestUserDto();
        when(userRepository.existsById(1L)).thenReturn(true);
        Executable executable = () -> userService.updateUser(userDto, 1L, "ivan_ivanov");
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, executable);
        String expected = String.format("400 BAD_REQUEST \"%s\"", "Не удалось обновить атрибуты пользователя с id = 1");
        String actual = String.format("%s", exception.getMessage());
        assertEquals(expected, actual);
    }

    @Test
    void deleteUserSuccess() {
        User user = createTestUser();
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        doNothing().when(userRepository).deleteById(1L);
        String result = userService.deleteUser(1L, "ivan_ivanov");
        String expected = "Пользователь с id = 1 успешно удален";
        assertEquals(expected, result);
    }

    @Test
    void deleteUserFail() {
        User user = createTestUser();
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        doThrow(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Не удалось удалить пользователя с id = 1"))
                .doNothing().when(userRepository).deleteById(1L);
        Executable executable = () -> userService.deleteUser(1L, "ivan_ivanov");
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, executable);
        String expected = String.format("400 BAD_REQUEST \"%s\"", "Не удалось удалить пользователя с id = 1");
        String actual = String.format("%s", exception.getMessage());
        assertEquals(expected, actual);
    }

    UserDto createTestUserDto() {
        return UserDto.builder()
                .id(1L)
                .firstName("Иван")
                .lastName("Иванов")
                .surName("Иванович")
                .birthDate(LocalDate.of(1995, 2, 10))
                .nikname("ivan_ivanov")
                .description("Описание пользователя")
                .build();
    }

}