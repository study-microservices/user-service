package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.dto.SkillDto;
import com.skillbox.microservices.users.entity.Skill;
import com.skillbox.microservices.users.repository.SkillRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class SkillServiceTest extends AbstractService {

    private final SkillRepository skillRepository = mock(SkillRepository.class);
    private final SkillService skillService = new SkillService(skillRepository, modelMapper);

    @Test
    void createSkillSuccess() {
        String name = "Java Core";
        SkillDto skillDto = SkillDto.builder()
                .name(name)
                .build();
        Skill skill = Skill.builder()
                .id(1)
                .name(name)
                .build();
        when(skillRepository.findByName(name)).thenReturn(null);
        when(skillRepository.save(any(Skill.class))).thenReturn(skill);
        String result = skillService.createSkill(skillDto);
        String expected = "Добавлен навык Java Core с id = 1";
        assertEquals(expected, result);
    }

    @Test
    void createSkillIfExist() {
        String name = "Java Core";
        SkillDto skillDto = SkillDto.builder()
                .name(name)
                .build();
        Skill skill = Skill.builder()
                .id(1)
                .name(name)
                .build();
        when(skillRepository.findByName(name)).thenReturn(skill);
        Executable executable = () -> skillService.createSkill(skillDto);
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, executable);
        String expected = String.format("400 BAD_REQUEST \"%s\"", "Навык Java Core уже существует");
        String actual = String.format("%s", exception.getMessage());
        assertEquals(expected, actual);
    }

    @Test
    void updateSkill() {
        String name = "Java Core";
        SkillDto skillDto = SkillDto.builder()
                .name(name)
                .build();
        Skill skill = convert(skillDto, Skill.class);
        when(skillRepository.existsById(1)).thenReturn(true);
        when(skillRepository.save(any(Skill.class))).thenReturn(skill);
        String result = skillService.updateSkill(skillDto, 1);
        String expected = String.format("Навык с id = %s успешно обновлен", skill.getId());
        assertEquals(expected, result);
    }

    @Test
    void deleteSkill() {
        when(skillRepository.existsById(1)).thenReturn(true);
        doNothing().when(skillRepository).deleteById(1);
        String result = skillService.deleteSkill(1);
        String expected = "Навык с id = 1 успешно удален";
        assertEquals(expected, result);
    }

    @Test
    void getAllSkills() {
        Skill skill = Skill.builder()
                .id(1)
                .name("Java Core")
                .build();
        List<Skill> skills = Collections.singletonList(skill);
        List<SkillDto> addressDtos = skills.stream()
                .map(a -> convert(a, SkillDto.class))
                .collect(Collectors.toList());
        when(skillRepository.findAll()).thenReturn(skills);
        List<SkillDto> result = skillService.getAllSkills();
        assertEquals(addressDtos.size(), result.size());
    }

    @Test
    void getSkillById() {
        Skill skill = Skill.builder()
                .id(1)
                .name("Java Core")
                .build();
        when(skillRepository.findById(1)).thenReturn(Optional.of(skill));
        Skill actual = skillService.getSkillById(1);
        assertEquals(skill, actual);
    }

    @Test
    void getSkillByIdNotFound() {
        when(skillRepository.findById(1)).thenReturn(Optional.ofNullable(null));
        Executable executable = () -> skillService.getSkillById(1);
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, executable);
        String expected = String.format("404 NOT_FOUND \"%s\"", "Навык с идентификатором 1 не найден");
        String actual = String.format("%s", exception.getMessage());
        assertEquals(expected, actual);
    }
}