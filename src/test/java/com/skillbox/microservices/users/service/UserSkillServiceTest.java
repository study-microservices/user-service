package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.dto.SkillDto;
import com.skillbox.microservices.users.entity.Skill;
import com.skillbox.microservices.users.entity.User;
import com.skillbox.microservices.users.entity.UserSkill;
import com.skillbox.microservices.users.repository.UserSkillRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserSkillServiceTest extends AbstractService {
    private final UserSkillRepository userSkillRepository = mock(UserSkillRepository .class);
    private final UserService userService = mock(UserService.class);
    private final SkillService skillService = mock(SkillService.class);
    private final UserSkillService userSkillService = new UserSkillService(userSkillRepository,
            userService, skillService, modelMapper);
    @Test
    void createUserSkill() {
        User user = createTestUser();
        Skill skill = Skill.builder()
                .id(1)
                .name("Java Core")
                .build();
        SkillDto skillDto = convert(skill, SkillDto.class);
        when(userService.getUserById(1L)).thenReturn(user);
        when(skillService.getSkillById(1)).thenReturn(skill);
        UserSkill userSkill = UserSkill.builder()
                .user(user)
                .skill(skill)
                .build();
        when(userSkillRepository.save(any(UserSkill.class))).thenReturn(userSkill);
        String result = userSkillService.createUserSkill(skillDto, 1L);
        String expected = "Пользователю с id = 1 добавлен навык с id = 1";
        assertEquals(expected, result);
    }

    @Test
    void getUserSkills() {
        Skill skill = Skill.builder()
                .id(1)
                .name("Java Core")
                .build();
        UserSkill userSkill = UserSkill.builder()
                .user(createTestUser())
                .skill(skill)
                .build();
        List<UserSkill> skills = Collections.singletonList(userSkill);
        doNothing().when(userService).checkUser(1L);
        when(userSkillRepository.findAllByUserId(1L)).thenReturn(skills);
        List<SkillDto> contactDtos = skills.stream()
                .map((uc) -> modelMapper.map(uc, SkillDto.class))
                .collect(Collectors.toList());
        assertFalse(contactDtos.isEmpty());
    }

    @Test
    void getUserSkillsFail() {
        doThrow(new ResponseStatusException(HttpStatus.NOT_FOUND,
                "Пользователь с id = 2 не найден")).when(userService).checkUser(2L);
        Executable executable = () -> userSkillService.getUserSkills(2L);
        assertThrows(ResponseStatusException.class, executable);
    }

    @Test
    void deleteUserSkill() {
        doNothing().when(userService).checkUser(1L);
        doNothing().when(userSkillRepository).deleteById(1);
        String result = userSkillService.deleteUserSkill(1L, 1);
        String expected = "У пользователя с id = 1 удален навык с id = 1";
        assertEquals(expected, result);
    }
}