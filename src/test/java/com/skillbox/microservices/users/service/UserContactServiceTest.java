package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.dto.UserContactDto;
import com.skillbox.microservices.users.entity.User;
import com.skillbox.microservices.users.entity.UserContact;
import com.skillbox.microservices.users.enums.ContactType;
import com.skillbox.microservices.users.repository.UserContactRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserContactServiceTest extends AbstractService {

    private final UserContactRepository userContactRepository = mock(UserContactRepository .class);
    private final UserService userService = mock(UserService.class);
    private final UserContactService userContactService = new UserContactService(userContactRepository,
            userService, modelMapper);

    @Test
    void getUserContactsSuccess() {
        UserContact userContact = UserContact.builder()
                .contact("admin@admin.com")
                .user(createTestUser())
                .contactType(ContactType.EMAIL)
                .build();
        List<UserContact> contacts = new ArrayList<>();
        contacts.add(userContact);
        doNothing().when(userService).checkUser(1L);
        when(userContactRepository.findAllByUserId(1L)).thenReturn(contacts);
        List<UserContactDto> contactDtos = contacts.stream()
                .map((uc) -> modelMapper.map(uc, UserContactDto.class))
                .collect(Collectors.toList());
        assertFalse(contactDtos.isEmpty());
    }

    @Test
    void getUserContactsFail() {
        doThrow(new ResponseStatusException(HttpStatus.NOT_FOUND,
                "Пользователь с id = 2 не найден")).when(userService).checkUser(2L);
        Executable executable = () -> userContactService.getUserContacts(2L);
        assertThrows(ResponseStatusException.class, executable);
    }

    @Test
    void createUserContact() {
        User user = createTestUser();
        when(userService.getUserById(1L)).thenReturn(user);
        UserContactDto userContactDto = UserContactDto.builder()
                .id(1)
                .contact("admin@admin.com")
                .contactType("EMAIL")
                .build();
        UserContact saved = UserContact.builder()
                .id(1)
                .contact(userContactDto.getContact())
                .contactType(ContactType.valueOf(userContactDto.getContactType()))
                .user(user)
                .build();
        when(userContactRepository.save(any(UserContact.class))).thenReturn(saved);
        String result = userContactService.createUserContact(userContactDto, 1L);
        String expected = "Пользователю с id = 1 добавлен контакт с id = 1";
        assertEquals(expected, result);
    }

    @Test
    void deleteUserContactSuccess() {
        doNothing().when(userService).checkUser(1L);
        doNothing().when(userContactRepository).deleteById(1);
        String result = userContactService.deleteUserContact(1L, 1);
        String expected = "У пользователя с id = 1 удален контакт с id = 1";
        assertEquals(expected, result);
    }

    @Test
    void deleteUserContactFail() {
        doNothing().when(userService).checkUser(1L);
        doThrow(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Не удалось удалить контакт у пользователя с id = 1"))
                .doNothing().when(userContactRepository).deleteById(1);
        Executable executable = () -> userContactService.deleteUserContact(1L, 1);
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, executable);
        String expected = String.format("400 BAD_REQUEST \"%s\"", "Не удалось удалить контакт у пользователя с id = 1");
        String actual = String.format("%s", exception.getMessage());
        assertEquals(expected, actual);
    }

}