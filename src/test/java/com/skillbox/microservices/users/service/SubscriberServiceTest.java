package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.dto.UserDto;
import com.skillbox.microservices.users.entity.Subscriber;
import com.skillbox.microservices.users.entity.User;
import com.skillbox.microservices.users.repository.SubscriberRepository;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class SubscriberServiceTest extends AbstractService {

    private final SubscriberRepository subscriberRepository = mock(SubscriberRepository.class);
    private final UserService userService = mock(UserService.class);
    private final SubscriberService subscriberService = new SubscriberService(subscriberRepository, userService);

    @Test
    void subscribe() {
        User user = createTestUser();
        User user2 = User.builder()
                .id(2L)
                .firstName("Петр")
                .lastName("Петров")
                .surName("Петрович")
                .birthDate(LocalDate.of(1998, 7, 6))
                .nikname("petr_petrov")
                .description("Описание пользователя")
                .build();
        when(userService.getUserById(1L)).thenReturn(user);
        when(userService.getUserById(2L)).thenReturn(user2);
        Subscriber subscriber = Subscriber
                .builder()
                .user(user)
                .subscriber(user2)
                .build();
        Subscriber saved = Subscriber
                .builder()
                .user(user)
                .subscriber(user2)
                .build();
        when(subscriberRepository.save(subscriber)).thenReturn(saved);
        String result = subscriberService.subscribe(convert(user2, UserDto.class), 1L);
        String expected = String.format("Пользователь %s подписался на пользователя с id = %s",
                user2.getFullName(), user.getId());
        assertEquals(expected, result);
    }

    @Test
    void unsubscribe() {
        User user = createTestUser();
        User user2 = User.builder()
                .id(2L)
                .firstName("Петр")
                .lastName("Петров")
                .surName("Петрович")
                .birthDate(LocalDate.of(1998, 7, 6))
                .nikname("petr_petrov")
                .description("Описание пользователя")
                .build();
        when(userService.getUserById(1L)).thenReturn(user);
        when(userService.getUserById(2L)).thenReturn(user2);
        doNothing().when(subscriberRepository).deleteByUserIdAndSubscriberId(2L, 1L);
        String result = subscriberService.unsubscribe(convert(user2, UserDto.class), 1L);
        String expected = String.format("Пользователь %s отписался от пользователя с id = %s",
                user2.getFullName(), user.getId());
        assertEquals(expected, result);
    }
}