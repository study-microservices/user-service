package com.skillbox.microservices.users.service;

import com.skillbox.microservices.users.dto.UserDto;
import com.skillbox.microservices.users.entity.Subscription;
import com.skillbox.microservices.users.entity.User;
import com.skillbox.microservices.users.repository.SubscriptionRepository;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class SubscriptionServiceTest extends AbstractService {

    private final SubscriptionRepository subscriptionRepository = mock(SubscriptionRepository.class);
    private final UserService userService = mock(UserService.class);
    private final SubscriptionService subscriptionService = new SubscriptionService(subscriptionRepository, userService);

    @Test
    void subscription() {
        User user = createTestUser();
        User user2 = User.builder()
                .id(2L)
                .firstName("Петр")
                .lastName("Петров")
                .surName("Петрович")
                .birthDate(LocalDate.of(1998, 7, 6))
                .nikname("petr_petrov")
                .description("Описание пользователя")
                .build();
        when(userService.getUserById(1L)).thenReturn(user);
        when(userService.getUserById(2L)).thenReturn(user2);
        Subscription subscription = Subscription
                .builder()
                .user(user)
                .subscription(user2)
                .build();
        Subscription saved = Subscription
                .builder()
                .user(user)
                .subscription(user2)
                .build();
        when(subscriptionRepository.save(subscription)).thenReturn(saved);
        String result = subscriptionService.subscription(convert(user, UserDto.class), 2L);
        String expected = String.format("Пользователь %s подписался на пользователя %s",
                user2.getFullName(), user.getFullName());
        assertEquals(expected, result);
    }

    @Test
    void unsubscription() {
        User user = createTestUser();
        User user2 = User.builder()
                .id(2L)
                .firstName("Петр")
                .lastName("Петров")
                .surName("Петрович")
                .birthDate(LocalDate.of(1998, 7, 6))
                .nikname("petr_petrov")
                .description("Описание пользователя")
                .build();
        when(userService.getUserById(1L)).thenReturn(user);
        when(userService.getUserById(2L)).thenReturn(user2);
        doNothing().when(subscriptionRepository).deleteByUserIdAndSubscriptionId(1L, 2L);
        String result = subscriptionService.unsubscription(convert(user, UserDto.class), 2L);
        String expected = String.format("Пользователь %s отписался от пользователя с id = %s",
                user2.getFullName(), user.getId());
        assertEquals(expected, result);
    }
}