FROM openjdk:11
# create app dir
RUN mkdir /app/
ENV SPRING_DATASOURCE_URL=""
ENV SPRING_DATASOURCE_USERNAME=""
ENV SPRING_DATASOURCE_PASSWORD=""
# open ports
EXPOSE 8080
# copy jar file
ADD build/libs/users_service.jar /app/users_service.jar
ENTRYPOINT ["java", "-jar", "/app/users_service.jar"]